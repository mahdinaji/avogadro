#Imports
import sys
import math


def main():
    ALL_VALID_RELOCATIONS_DISTANCE = sys.argv[1:] # getting all arguments except first one

    # read from file if there is no arguments
    if len(sys.argv) == 1:
        ALL_VALID_RELOCATIONS_DISTANCE=[]
        with open('./input.txt', 'r') as f:
            for line in f:
                try:
                    ALL_VALID_RELOCATIONS_DISTANCE.append(float(line.strip()))
                except:
                    pass
    
    sum_of_squares = 0
    for i in ALL_VALID_RELOCATIONS_DISTANCE:
        sum_of_squares += float( ((7e-6)/40) * i ) ** 2 # accumulating each relocation's distance's square
    
    D = sum_of_squares / (2 * len(ALL_VALID_RELOCATIONS_DISTANCE))

    # the constants
    R = 8.31446
    T = 297
    n = 9.135e-4
    p = .5e-6

    K = ( D * 6 * math.pi * n * p ) / T # Boltzmann

    The_Avogadro = R / K

    print("Boltzmann = %e"%(K))
    print("Avogadro = %e"%(The_Avogadro))


# python Avogadro.py
# will process the distances from input.txt file
# input.txt file is the BeadTracker.py output file
if __name__ == "__main__":
    main()





    
