#Imports
import sys
import math


class Blob:
    """
    A class for representing Blobs

    Attributes
    ----------
    x : float
        distance from the y axis
    y : float
        distance from the x axis

    _mass : float
        total size of blob
    
    Methods
    -------
    add(x=0, y=0)
        add new point to blob and returns new total mass
    mass()
        returns total mass of blob
    distanceTo(Blob)
        returns distance to second Blob per pixel
    """
    def __init__(self):
        # initializes a new Blob
        self._mass = .0
        self.x = .0
        self.y = .0
    
    def add(self, x=.0, y=.0):
        """add new point to blob and returns new total mass
        Parameters
        ----------
        x : float
            distance from the y axis
        y : float
            distance from the x axis
        
        returns
        -------
        float
            total size of blob
        """
        currentMass = self._mass # the mass before adding
        # currentX = self.x
        # currentY = self.y
        self._mass += 1.0
        # adding new point's effect on Blob's position
        self.x = ((currentMass * self.x) + float(x)) / self._mass
        self.y = ((currentMass * self.y) + float(y)) / self._mass
        # new position is mean of all points position
        return self._mass

    def mass(self):
        """returns total mass of blob
        returns
        -------
        int
            total mass of blob
        """
        return int(self._mass)

    def distanceTo(self, c):
        """returns distance to second Blob per pixel
        Parameters
        ----------
        c : Blob
            second Blob
        returns
        -------
        float
            distance to second Blob per pixel
        """
        deltaX = self.x - c.x
        deltaY = self.y - c.y
        # Euclidean distance between self and c per pixels
        return math.sqrt(deltaX ** 2 + deltaY ** 2)

    def __str__(self):
        # returns formatted string as "Blob.mass (Blob.x, Blob.y)"
        return ('%d (%.4f, %.4f)' % (self._mass, self.x, self.y))

def main():
    # initialize a new Blob
    tempBlob = Blob()

    # default value of Point0's position
    x0 = 2
    y0 = 3
    # default value of Point1's position
    x1 = 3
    y1 = 2

    # overrides Point0's position if exists in arguments
    if len(sys.argv) > 2:
        x0 = float(sys.argv[1])
        y0 = float(sys.argv[2])
    tempBlob.add(x0, y0)

    # overrides Point1's position if exists in arguments
    if len(sys.argv) > 4:
        x1 = float(sys.argv[3])
        y1 = float(sys.argv[4])
    tempBlob.add(x1, y1)

    # prints current Blob state
    print(str(tempBlob))
    return 1

if __name__ == "__main__":
    main()