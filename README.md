Estimating avogadro's by boltzmann constant


Blob.py is a model for each blob in the image

contains size and location of the blob in image



BeadFinder.py should detect blobs in image with given parameters

Example:

python BeadFinder.py run_1\frame00000.jpg 10 128

will output list of Blobs detected with minimum of 10 pixels size and with tau=128



BeadTracker.py tracks each blob in pair of images in order and calculates relocation distance

Example:

python BeadTracker.py 20 128 5 run_1\\*

will find distances of pair blobs relocated in pair of images in order

all detected blobs will be at least 20 pixels big and maximum distance between two blobs in each image is 5 pixels

and tau is 128



Avogadro.py reads the distances from std input or the input.txt file in current directory and calculates Avogadro's constant


more run images can be found at https://github.com/snrazavi/python-2019


stdarray.py picture.py color.py stdio.py are from https://introcs.cs.princeton.edu/python
input.txt is a result of a similar BeadTracker from https://github.com/snrazavi/python-2019