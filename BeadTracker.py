#Imports
import sys
import glob
import math
from BeadFinder import BeadFinder
from Blob import Blob
from picture import Picture
from color import Color


def main():
    # check arguments count
    if len(sys.argv) < 5:
        return 0

    # getting arguments
    try:
        min_pixels = int(sys.argv[1])
        tau = float(sys.argv[2])
        delta = float(sys.argv[3])
        imagesAddress = sys.argv[4]    
    except:
        return 0
    
    # get all files matching pattern as array and sort them
    picturesFilenameArray = sorted(glob.glob(imagesAddress))

    picturesCount = len(picturesFilenameArray)
    
    # at least two images are required
    if picturesCount < 2:
        print("no files found in " + imagesAddress)
    
    # output array of valid relocations for saving into file
    ALL_VALID_RELOCATIONS_DISTANCE = []

    # track beads in each pair of images
    for fileIndex in range(picturesCount - 1):
        # showing progress
        # print("%d / %d" % (fileIndex, picturesCount))

        # finding all blobs in images
        firstImageBeadFinder = BeadFinder(picturesFilenameArray[fileIndex], tau)
        secondImageBeadFinder = BeadFinder(picturesFilenameArray[fileIndex+1], tau)
        
        # finding all beads with minimum pixels inside blobs
        firstImageBeads = firstImageBeadFinder.getBeads(min_pixels)
        secondImageBeads = secondImageBeadFinder.getBeads(min_pixels)      

        # all possible relocations of beads from first image to second
        # allRelocations is two dimensional matrix
        allRelocations = [[]]
        # initializing matrix
        for i in range(len(firstImageBeads) - 1):
            allRelocations.append([])
        
        # constructing matrix, each row represents distance between each bead of first image and second image's beads
        for i in range(len(firstImageBeads)):
            for j in range(len(secondImageBeads)):
                allRelocations[i].append(firstImageBeads[i].distanceTo(secondImageBeads[j]))

        # keep searching until the all possible relocations are checked
        # impossible relocations will be removed from allRelocations each loop
        while len(allRelocations) != 0 and len(allRelocations[0]) != 0:
            
            for firstImageBeadIndex in range(len(allRelocations)):
                # assume the minimum is the first one and save its index
                secondImageNearestBead = allRelocations[firstImageBeadIndex][0]
                secondImageNearestBeadIndex = 0
                
                #check if the current minimum is less tha delta
                oneOfRelocationsAreValid = secondImageNearestBead <= delta

                # search for nearest bead in second image to current bead in first image
                for secondImageBeadIndex in range(1, len(allRelocations[firstImageBeadIndex])):
                    if allRelocations[firstImageBeadIndex][secondImageBeadIndex] < secondImageNearestBead: # this one is closer
                        secondImageNearestBead = allRelocations[firstImageBeadIndex][secondImageBeadIndex] # let's assume this one is the minimum and save its index
                        secondImageNearestBeadIndex = secondImageBeadIndex
                        if secondImageNearestBead <= delta: # check if the current minimum is less tha delta
                            oneOfRelocationsAreValid = True
                
                # if there is no distance less tha delta in current row,
                # so current row's bead(in first image) is destroyed in next image
                # since the current row's bead is not going to connect to any other beads in second image
                # we can delete it from matrix in order to decrease the processing.
                if oneOfRelocationsAreValid != True:
                    del allRelocations[firstImageBeadIndex]
                    firstImageBeadIndex=len(allRelocations) # set the counter of last for recent to end and begin from start
                    break # break the last for loop [ for firstImageBeadIndex in range(len(allRelocations)): ]

                # if the closest bead in second image to current bead in first image, is not closer to any other
                # beads in first image, it's a valid relocation. here we should search for any closer bead in first image to current closest bead in second image
                currentRelocationIsConfirmed = True
                for i in range(len(allRelocations)):
                    if allRelocations[i][secondImageNearestBeadIndex] < secondImageNearestBead: # search for beads in column(first image)
                        currentRelocationIsConfirmed = False # there is at least one bead in first image that is closer to second image's bead than current first image's bead
                        break # break the current loop and move on
                
                # if there is no closer beads in first image than current one to second image's bead
                # so the current bead in first image is the other bead in second image and this relocation is valid
                if currentRelocationIsConfirmed == True:
                    print("%.4f" % (secondImageNearestBead)) # print the current distance between two beads from each images
                    ALL_VALID_RELOCATIONS_DISTANCE.append(secondImageNearestBead) # add this distance to output array for saving into file
                    
                    # remove second image's bead from matrix, its distances from first image's beads are saved in column
                    for Row in range(len(allRelocations)):
                        del allRelocations[Row][secondImageNearestBeadIndex]
                    # also delete the current bead in first image from matrix
                    del allRelocations[firstImageBeadIndex]
                    firstImageBeadIndex=len(allRelocations) # just like before set the counter of recent for loop to end and begin from start
                    break # break the last for loop [ for firstImageBeadIndex in range(len(allRelocations)): ]
                
                # end of for loop
                # continue to search closest beads from second image to first image's beads in next row (each row is distances of second image's beads to current row's equivalent first image's bead)
            # end if while loop
            # start from begining of matrix to find closest pair of beads in each images
            # everytime the matrix is getting smaller
        # end of for loop (loading pair of images)
        # valid relocations are printed in stdio and are saved in ALL_VALID_RELOCATIONS_DISTANCE array

        # end of pair image distances flag for file output
        ALL_VALID_RELOCATIONS_DISTANCE.append(-1)
    
    # saving ALL_VALID_RELOCATIONS_DISTANCE array to file
    with open('BeadTrackerResults.txt', 'w') as filehandle:  
        for validRelocation in ALL_VALID_RELOCATIONS_DISTANCE:
            if validRelocation != -1:
                if validRelocation<10:
                    filehandle.write(' %.4f\n' % validRelocation)
                else:
                    filehandle.write('%.4f\n' % validRelocation)
            else:
                filehandle.write('\n')
    # end of main function


# python BeadTracker.py 20 128 5 run_1\*
# will find distances of pair blobs relocated in pair of images in order
# all detected blobs will be at lease 20 pixels big and maximum distance between two blobs in each image is 5 pixels
# and tau is 128
# different configurations will generate different outputs
if __name__ == "__main__":
    main()