#Imports
import sys
from Blob import Blob
from picture import Picture
from color import Color


class BeadFinder():
    """
    A class for representing found beads
    after construction object contains an array of all Blobs with brightness more than tau

    Parameters
    ----------
    picture : str
        filename fo current Picture to find Beads in
    tau : float
        threshold of Bead's brightness, a value between 0 and 255

    Attributes
    ----------
    _allBlobsFound : array of Blob
        all blobs with brightness more than tau
    _tau : float
        default tau of class
    _currentPicture : Picture
        temporary Picture element of class

    Methods
    -------
        Nothing yet!!!
    """


    def __init__(self, picture, tau=128.0):
        self._allBlobsFound = [] # array of Blobs
        # load _currentPicture from picture (picture from params is filename)
        self._currentPicture = Picture(picture)
        self._tau = tau
        
        # exploring bright dots in _currentPicture and expanding Blobs covering these dots
        for x in range(self._currentPicture.width()):
            for y in range(self._currentPicture.height()):
                currentPixelColor = self._currentPicture.get(x, y)
                if currentPixelColor.getRed() * .3 + currentPixelColor.getGreen() * .59 + currentPixelColor.getBlue() * .11 >= self._tau:
                    # make a new Blob for current bright dot
                    # next bright dot found here will be seperated from this Blob at least by a line of black dots between
                    self._allBlobsFound.append(Blob())

                    # expanding a bright dot makes all connected dots black
                    self._setPixelBlack(x, y)

                    # find nearest bright dots to current one and continue
                    self._Expand(x, y, len(self._allBlobsFound) - 1)
        
        # remove _currentPicture from memory
        # it's not gonna be used anymore
        del self._currentPicture

    
    def _Expand(self, x, y, currentBlobIndex):
        """a recursive function to search for bright dots around point(x, y) and add them to Blobs array at index of currentBlobIndex
        
        Parameters
        ----------
        x : float
            distance from the y axis
        y : float
            distance from the x axis
        currentBlobIndex : int
            index of Blob to add next bright dots into it

        """
        # make current point black to avoid beeing Expanded later
        # self._setPixelBlack(x, y)

        # add current dot to current blob
        self._allBlobsFound[currentBlobIndex].add(x, y)
        maximumWidth = self._currentPicture.width()
        maximumHeight = self._currentPicture.height()
        whiteMarginX = []
        whiteMarginY = []

        # explore dots around
        for i in range(x-2, x+3):
            for j in range(y-2, y+3):
                if i != x and j != y: # except dot itself
                    if i >= 0 and j >= 0 and i < maximumWidth and j < maximumHeight: # only if dot is inside _currentPicture
                        currentPixelColor = self._currentPicture.get(i, j)
                        if currentPixelColor.getRed()*.3 + currentPixelColor.getGreen()*.59+ currentPixelColor.getBlue()*.11 >= self._tau:
                            # make bright dots black, looks like a memoization
                            # so there is no need to new Blob and expand it anymore
                            self._setPixelBlack(i, j)
                            whiteMarginX.append(i) # add to Expand queue
                            whiteMarginY.append(j)

        # expand all bright dots in queue
        for i in range(len(whiteMarginX)):
            # expand all connected bright dots around and add them to current Blob
            self._setPixelBlack(whiteMarginX[i], whiteMarginY[i])
            self._Expand(whiteMarginX[i], whiteMarginY[i], currentBlobIndex)


    def _setPixelBlack(self, x, y):
        """sets the point(x, y) of _currentPicture black

        Parameters
        ----------
        x : float
            distance from the y axis
        y : float
            distance from the x axis
        """
        if x >= 0 and y >= 0 and x < self._currentPicture.width() and y < self._currentPicture.height(): # only if dot is inside _currentPicture
            self._currentPicture.set(x, y, Color(0, 0, 0))
    

    def getBeads(self, min_pixels):
        """returns array of beads with minimum pixels of min_pixels inside

        Parameters
        ----------
        min_pixels : int
            minumum pixels inside beads

        returns
        -------
        ArrayOfBeads : array of Blob
            blobs with mass more than min_pixels
        """
        returnArrayOfBeads = []
        for i in range(len(self._allBlobsFound)): # check all blobs in _currentPicture
            if self._allBlobsFound[i].mass() > int(min_pixels): # add blobs with mass more than min_pixels to returnArrayOfBeads array
                returnArrayOfBeads.append(self._allBlobsFound[i])
        
        return returnArrayOfBeads



def main():
    temp = BeadFinder(sys.argv[1], int(sys.argv[3]))
    currentBeads = temp.getBeads(int(sys.argv[2]))
    for i in range(len(currentBeads)):
        print(currentBeads[i])


# python BeadFinder.py run_1\frame00000.jpg 10 128
# will output list of Blobs detected with minimum of 10 pixels size and with tau=128
if __name__ == "__main__":
    main()